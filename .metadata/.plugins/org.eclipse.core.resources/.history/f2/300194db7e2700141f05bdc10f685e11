package com.example.todo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.R.string;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
	public int mHour;
	public int mMinute;
	public int mSecond;
	public int mDay, mMonth, mYear;
	// Logcat tag
	private static final String LOG = DatabaseHelper.class.getName();

	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "toDoListManager";

	// Table Names
	private static final String TABLE_TASKS = "tasks";
	// NOTES Table - column names
	private static final String KEY_TODO = "todo";
	private static final String KEY_STATUS = "status";
	private static final String KEY_ID = "id";
	private static final String KEY_CREATED_AT = "created_at";
	private static final String KEY_ISSETALARM = "is_set_alarm";
	private static final String KEY_ALARMTIME = "alarm_time";

	// Table Create Statements
	// Tasks table create statement
	private static final String CREATE_TABLE_TASKS = "CREATE TABLE "+ TABLE_TASKS + "(" 
			+ KEY_ID + " INTEGER PRIMARY KEY," 
			+ KEY_TODO + " TEXT," 
			+ KEY_STATUS + " INTEGER," 
			+ KEY_CREATED_AT + " DATETIME, " 
			+ KEY_ISSETALARM + " INTEGER, " 
			+ KEY_ALARMTIME + " TEXT " 
			+ ")";

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		// creating required tables
		db.execSQL(CREATE_TABLE_TASKS);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// on upgrade drop older tables
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TASKS);

		// create new tables
		onCreate(db);
	}

	// ------------------------ "tasks" table methods ----------------//

	/**
	 * Creating a task
	 */
	public long createToDo(Tasks tasks) {
		
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_TODO, tasks.getNote());
		values.put(KEY_STATUS, tasks.getStatus());
		values.put(KEY_CREATED_AT, getDateTime());
		values.put(KEY_ISSETALARM, tasks.getIsSetAlarm());
		values.put(KEY_ALARMTIME, getAlarmTime());
		// insert row
		long todo_id = db.insert(TABLE_TASKS, null, values);
		return todo_id;
	}

	/**
	 * get single task
	 */
	public Tasks getTask(long tasks_id) {
		SQLiteDatabase db = this.getReadableDatabase();

		String selectQuery = "SELECT  * FROM " + TABLE_TASKS + " WHERE "
				+ KEY_ID + " = " + tasks_id;

		Log.e(LOG, selectQuery);

		Cursor c = db.rawQuery(selectQuery, null);

		if (c != null)
			c.moveToFirst();

		Tasks td = new Tasks();
		td.setId(c.getInt(c.getColumnIndex(KEY_ID)));
		td.setNote((c.getString(c.getColumnIndex(KEY_TODO))));
		td.setCreatedAt(c.getString(c.getColumnIndex(KEY_CREATED_AT)));
		td.setIsSetAlarm(c.getInt(c.getColumnIndex(KEY_ISSETALARM)));
		td.setAlarmTime(c.getString(c.getColumnIndex(KEY_ALARMTIME)));
		return td;
	}

	/**
	 * getting all Tasks
	 * */
	public List<Tasks> getAllTasks() {
		List<Tasks> tasks = new ArrayList<Tasks>();
		String selectQuery = "SELECT  * FROM " + TABLE_TASKS;

		Log.i(LOG, selectQuery);

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				Tasks td = new Tasks();
				td.setId(c.getInt((c.getColumnIndex(KEY_ID))));
				td.setNote((c.getString(c.getColumnIndex(KEY_TODO))));
				td.setCreatedAt(c.getString(c.getColumnIndex(KEY_CREATED_AT)));
				td.setIsSetAlarm(c.getInt(c.getColumnIndex(KEY_ISSETALARM)));
				td.setAlarmTime(c.getString(c.getColumnIndex(KEY_ALARMTIME)));
				// adding to Tasks list
				tasks.add(td);
			} while (c.moveToNext());
		}

		return tasks;
	}

	/**
	 * getting Tasks count
	 */
	public int getTasksCount() {
		String countQuery = "SELECT  * FROM " + TABLE_TASKS;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);

		int count = cursor.getCount();
		cursor.close();

		
		return count;
	}

	/**
	 * Updating a Tasks
	 */
	public int updateToDo(Tasks todo) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_TODO, todo.getNote());
		values.put(KEY_STATUS, todo.getStatus());

		// updating row
		return db.update(TABLE_TASKS, values, KEY_ID + " = ?",
				new String[] { String.valueOf(todo.getId()) });
	}

	/**
	 * Deleting a Tasks
	 */
	public void deleteToDo(long tado_id) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_TASKS, KEY_ID + " = ?",
				new String[] { String.valueOf(tado_id) });
	}

	// closing database
	public void closeDB() {
		SQLiteDatabase db = this.getReadableDatabase();
		if (db != null && db.isOpen())
			db.close();
	}

	/**
	 * get datetime
	 * */
	private String getDateTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"HH:mm:ss dd-MM-yyyy", Locale.getDefault());
		Date date = new Date();
		return dateFormat.format(date);
	}

	public void getTime(int hour, int minute, int second) {
		mHour = hour;
		mMinute = minute;
		mSecond = second;
	}

	public void getDate(int day, int month, int year) {
		mDay = day;
		mMonth = month;
		mYear = year;
	}
	private String getAlarmTime() {
		String time = mHour + ":" + mMinute + ":"
				+ mSecond;
		String date = mDay + "-" + mMonth + "-"
				+ mYear;
		String dateTime = time + " , " + date;
		return dateTime;

	}
}