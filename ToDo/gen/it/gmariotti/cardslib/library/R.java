/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package it.gmariotti.cardslib.library;

public final class R {
	public static final class attr {
		public static final int card_header_layout_resourceID = 0x7f010022;
		public static final int card_layout_resourceID = 0x7f010020;
		public static final int card_list_item_dividerHeight = 0x7f010025;
		public static final int card_shadow_layout_resourceID = 0x7f010021;
		public static final int card_thumbnail_layout_resourceID = 0x7f010023;
		public static final int list_card_layout_resourceID = 0x7f010024;
	}
	public static final class color {
		public static final int card_activated = 0x7f05001d;
		public static final int card_activated_kitkat = 0x7f050020;
		public static final int card_background = 0x7f050017;
		public static final int card_backgroundExpand = 0x7f050019;
		public static final int card_background_header = 0x7f050018;
		public static final int card_base_cardwithlist_background_list_color = 0x7f050023;
		public static final int card_base_cardwithlist_divider_color = 0x7f050022;
		public static final int card_expand_title_color = 0x7f05001b;
		public static final int card_foreground_activated = 0x7f05001e;
		public static final int card_foreground_activated_kitkat = 0x7f050021;
		public static final int card_pressed = 0x7f05001c;
		public static final int card_pressed_kitkat = 0x7f05001f;
		public static final int card_section_container_color = 0x7f050024;
		public static final int card_section_title_color = 0x7f050025;
		public static final int card_text_color_header = 0x7f05001a;
	}
	public static final class dimen {
		public static final int card_background_default_radius = 0x7f090001;
		public static final int card_base_cardwithlist_dividerHeight = 0x7f090032;
		public static final int card_base_cardwithlist_layout_leftmargin = 0x7f09002e;
		public static final int card_base_cardwithlist_layout_rightmargin = 0x7f09002f;
		public static final int card_base_cardwithlist_list_margin_left = 0x7f090031;
		public static final int card_base_cardwithlist_list_margin_top = 0x7f090030;
		public static final int card_base_empty_height = 0x7f090000;
		public static final int card_content_outer_view_margin_bottom = 0x7f090017;
		public static final int card_content_outer_view_margin_left = 0x7f090016;
		public static final int card_content_outer_view_margin_right = 0x7f090018;
		public static final int card_content_outer_view_margin_top = 0x7f090015;
		public static final int card_expand_layout_padding = 0x7f090022;
		public static final int card_expand_simple_title_paddingLeft = 0x7f090023;
		public static final int card_expand_simple_title_paddingRight = 0x7f090024;
		public static final int card_expand_simple_title_text_size = 0x7f090025;
		public static final int card_header_button_margin_right = 0x7f09000e;
		public static final int card_header_button_overflow_margin_right = 0x7f09000f;
		public static final int card_header_button_padding_bottom = 0x7f09000c;
		public static final int card_header_button_padding_left = 0x7f09000a;
		public static final int card_header_button_padding_right = 0x7f09000b;
		public static final int card_header_button_padding_top = 0x7f09000d;
		public static final int card_header_outer_view_margin_bottom = 0x7f090008;
		public static final int card_header_outer_view_margin_left = 0x7f090007;
		public static final int card_header_outer_view_margin_right = 0x7f090009;
		public static final int card_header_outer_view_margin_top = 0x7f090006;
		public static final int card_header_simple_title_margin_bottom = 0x7f090014;
		public static final int card_header_simple_title_margin_left = 0x7f090010;
		public static final int card_header_simple_title_margin_right = 0x7f090013;
		public static final int card_header_simple_title_margin_top = 0x7f090011;
		public static final int card_header_simple_title_text_size = 0x7f090012;
		public static final int card_main_layout_view_margin_bottom = 0x7f090004;
		public static final int card_main_layout_view_margin_left = 0x7f090003;
		public static final int card_main_layout_view_margin_right = 0x7f090005;
		public static final int card_main_layout_view_margin_top = 0x7f090002;
		public static final int card_main_simple_title_margin_left = 0x7f090019;
		public static final int card_main_simple_title_margin_top = 0x7f09001a;
		public static final int card_section_container_padding_left = 0x7f090034;
		public static final int card_section_container_padding_right = 0x7f090035;
		public static final int card_section_title = 0x7f090033;
		public static final int card_section_title_margin_top = 0x7f090036;
		public static final int card_shadow_height = 0x7f09001b;
		public static final int card_shadow_view_margin_bottom = 0x7f09001e;
		public static final int card_shadow_view_margin_left = 0x7f09001d;
		public static final int card_shadow_view_margin_right = 0x7f09001f;
		public static final int card_shadow_view_margin_top = 0x7f09001c;
		public static final int card_thumbnail_height = 0x7f090021;
		public static final int card_thumbnail_width = 0x7f090020;
		public static final int grid_card_padding_bottom = 0x7f09002c;
		public static final int grid_card_padding_left = 0x7f09002a;
		public static final int grid_card_padding_right = 0x7f09002b;
		public static final int grid_card_padding_top = 0x7f09002d;
		public static final int list_card_padding_bottom = 0x7f090028;
		public static final int list_card_padding_left = 0x7f090026;
		public static final int list_card_padding_right = 0x7f090027;
		public static final int list_card_padding_top = 0x7f090029;
	}
	public static final class drawable {
		public static final int activated_background_card = 0x7f020000;
		public static final int activated_background_kitkat_card = 0x7f020001;
		public static final int activated_foreground_card = 0x7f020002;
		public static final int activated_foreground_kitkat_card = 0x7f020003;
		public static final int card_background = 0x7f020007;
		public static final int card_foreground_kitkat_selector = 0x7f020008;
		public static final int card_foreground_selector = 0x7f020009;
		public static final int card_kitkat_selector = 0x7f02000a;
		public static final int card_menu_button_expand = 0x7f02000b;
		public static final int card_menu_button_overflow = 0x7f02000c;
		public static final int card_menu_button_rounded_overflow = 0x7f02000d;
		public static final int card_multichoice_selector = 0x7f02000e;
		public static final int card_selector = 0x7f02000f;
		public static final int card_shadow = 0x7f020010;
		public static final int card_undo = 0x7f020011;
		public static final int ic_menu_expand_card_dark_normal = 0x7f02002d;
		public static final int ic_menu_expand_card_dark_pressed = 0x7f02002e;
		public static final int ic_menu_overflow_card_dark_normal = 0x7f02002f;
		public static final int ic_menu_overflow_card_dark_pressed = 0x7f020030;
		public static final int ic_menu_overflow_card_rounded_dark_normal = 0x7f020031;
		public static final int ic_menu_overflow_card_rounded_dark_pressed = 0x7f020032;
		public static final int ic_undobar_undo = 0x7f020037;
		public static final int pressed_background_card = 0x7f02003b;
		public static final int pressed_background_kitkat_card = 0x7f02003c;
		public static final int undobar = 0x7f02003e;
		public static final int undobar_button_focused = 0x7f02003f;
		public static final int undobar_button_pressed = 0x7f020040;
		public static final int undobar_divider = 0x7f020041;
	}
	public static final class id {
		public static final int card_base_empty_cardwithlist_text = 0x7f070027;
		public static final int card_children_simple_title = 0x7f070023;
		public static final int card_content_expand_layout = 0x7f07002c;
		public static final int card_expand_inner_simple_title = 0x7f070039;
		public static final int card_header_button_expand = 0x7f070021;
		public static final int card_header_button_frame = 0x7f07001f;
		public static final int card_header_button_other = 0x7f070022;
		public static final int card_header_button_overflow = 0x7f070020;
		public static final int card_header_inner_frame = 0x7f07001e;
		public static final int card_header_inner_simple_title = 0x7f07003a;
		public static final int card_header_layout = 0x7f07002b;
		public static final int card_inner_base_empty_cardwithlist = 0x7f07003d;
		public static final int card_inner_base_main_cardwithlist = 0x7f07003c;
		public static final int card_inner_base_progressbar_cardwithlist = 0x7f07003e;
		public static final int card_main_content_layout = 0x7f070028;
		public static final int card_main_inner_simple_title = 0x7f07003b;
		public static final int card_main_layout = 0x7f07002a;
		public static final int card_overlap = 0x7f07002d;
		public static final int card_section_simple_title = 0x7f070024;
		public static final int card_shadow_layout = 0x7f070029;
		public static final int card_shadow_view = 0x7f070025;
		public static final int card_thumb_and_content_layout = 0x7f07002e;
		public static final int card_thumbnail_image = 0x7f070026;
		public static final int card_thumbnail_layout = 0x7f07002f;
		public static final int list_cardId = 0x7f07003f;
		public static final int list_card_undobar = 0x7f070040;
		public static final int list_card_undobar_button = 0x7f070042;
		public static final int list_card_undobar_message = 0x7f070041;
		public static final int undobar = 0x7f070030;
		public static final int undobar_button = 0x7f070032;
		public static final int undobar_message = 0x7f070031;
	}
	public static final class integer {
		public static final int list_card_swipe_distance_divisor = 0x7f080001;
		public static final int list_card_undobar_hide_delay = 0x7f080002;
	}
	public static final class layout {
		public static final int base_header_layout = 0x7f030002;
		public static final int base_list_expandable_children_layout = 0x7f030003;
		public static final int base_section_layout = 0x7f030004;
		public static final int base_shadow_layout = 0x7f030005;
		public static final int base_thumbnail_layout = 0x7f030006;
		public static final int base_withlist_empty = 0x7f030007;
		public static final int base_withlist_progress = 0x7f030008;
		public static final int card_base_layout = 0x7f030009;
		public static final int card_layout = 0x7f03000a;
		public static final int card_overlay_layout = 0x7f03000b;
		public static final int card_thumbnail_layout = 0x7f03000c;
		public static final int card_thumbnail_overlay_layout = 0x7f03000d;
		public static final int card_undo_layout = 0x7f03000e;
		public static final int inner_base_expand = 0x7f030010;
		public static final int inner_base_header = 0x7f030011;
		public static final int inner_base_main = 0x7f030012;
		public static final int inner_base_main_cardwithlist = 0x7f030013;
		public static final int list_card_layout = 0x7f030014;
		public static final int list_card_thumbnail_layout = 0x7f030015;
		public static final int list_card_undo_message = 0x7f030016;
	}
	public static final class plurals {
		public static final int card_selected_items = 0x7f0a0001;
		public static final int list_card_undo_items = 0x7f0a0000;
	}
	public static final class string {
		public static final int card_empty_cardwithlist_text = 0x7f06001b;
		public static final int card_progressbar_cardwithlist_text = 0x7f06001c;
		public static final int list_card_undo_title = 0x7f06001a;
	}
	public static final class style {
		public static final int card = 0x7f040005;
		public static final int card_base_simple_title = 0x7f040011;
		public static final int card_content_outer_layout = 0x7f040010;
		public static final int card_expand_simple_title = 0x7f040017;
		public static final int card_header_button_base = 0x7f04000c;
		public static final int card_header_button_base_expand = 0x7f04000e;
		public static final int card_header_button_base_other = 0x7f04000f;
		public static final int card_header_button_base_overflow = 0x7f04000d;
		public static final int card_header_button_frame = 0x7f04000b;
		public static final int card_header_compound_view = 0x7f040009;
		public static final int card_header_outer_layout = 0x7f040008;
		public static final int card_header_simple_title = 0x7f04000a;
		public static final int card_main_contentExpand = 0x7f040016;
		public static final int card_main_layout = 0x7f040012;
		public static final int card_main_layout_foreground = 0x7f040014;
		public static final int card_main_layout_foreground_kitkat = 0x7f040015;
		public static final int card_main_layout_kitkat = 0x7f040013;
		public static final int card_section_container = 0x7f040020;
		public static final int card_section_title = 0x7f040021;
		public static final int card_shadow_image = 0x7f040007;
		public static final int card_shadow_outer_layout = 0x7f040006;
		public static final int card_thumbnail_compound_view = 0x7f04001a;
		public static final int card_thumbnail_image = 0x7f040018;
		public static final int card_thumbnail_outer_layout = 0x7f040019;
		public static final int cardwithlist = 0x7f04001f;
		public static final int grid_card = 0x7f04001e;
		public static final int list_card = 0x7f04001b;
		public static final int list_card_UndoBar = 0x7f040022;
		public static final int list_card_UndoBarButton = 0x7f040024;
		public static final int list_card_UndoBarMessage = 0x7f040023;
		public static final int list_card_base = 0x7f04001c;
		public static final int list_card_thumbnail = 0x7f04001d;
	}
	public static final class styleable {
		public static final int[] ForegroundLinearLayout = { 0x01010109, 0x01010200, 0x0101042e };
		public static final int ForegroundLinearLayout_android_foreground = 0;
		public static final int ForegroundLinearLayout_android_foregroundGravity = 1;
		public static final int ForegroundLinearLayout_android_foregroundInsidePadding = 2;
		public static final int[] card_listItem = { 0x7f010025 };
		public static final int card_listItem_card_list_item_dividerHeight = 0;
		public static final int[] card_options = { 0x7f010020, 0x7f010021, 0x7f010022, 0x7f010023, 0x7f010024 };
		public static final int card_options_card_header_layout_resourceID = 2;
		public static final int card_options_card_layout_resourceID = 0;
		public static final int card_options_card_shadow_layout_resourceID = 1;
		public static final int card_options_card_thumbnail_layout_resourceID = 3;
		public static final int card_options_list_card_layout_resourceID = 4;
	}
}
