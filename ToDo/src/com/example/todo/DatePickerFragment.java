package com.example.todo;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TextView;

public class DatePickerFragment extends DialogFragment implements
		DatePickerDialog.OnDateSetListener {
	int year;
	int month;
	int day;
	Context context;
	TextView mDate;
	AddTaskActivity mAddTask;
	DatabaseHelper mDatabaseHelper;
	public DatePickerFragment(TextView date,AddTaskActivity addTask,DatabaseHelper databaseHelper) {
		mDate=date;
		mAddTask=addTask;
		mDatabaseHelper=databaseHelper;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current date as the default date in the picker
		final Calendar calendar = Calendar.getInstance();
		year = calendar.get(Calendar.YEAR);
		month = calendar.get(Calendar.MONTH);
		day = calendar.get(Calendar.DAY_OF_MONTH);

		// Create a new instance of DatePickerDialog and return it
		return new DatePickerDialog(getActivity(), this, year, month, day);
	}

	@Override
	public void onDateSet(DatePicker view, int year, int month, int day) {
		mDate.setText("Date:	"+day+"-"+month+"-"+year);
		mAddTask.getDate(day,month,year);
		mDatabaseHelper.getDate(day,month,year);
	}

}