package com.example.todo;
 
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
 
public class MyReceiver extends BroadcastReceiver
{
      
    @Override
    public void onReceive(Context context, Intent intent)
    {   
    	int id = intent.getExtras().getInt("id");
       Intent service1 = new Intent(context, MyAlarmService.class);
       service1.putExtra("id", id);
       context.startService(service1);
        
    }  
}
