package com.example.todo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class TaskDetail extends Activity {
	  Button deleteBtn,editBtn;
	  TextView schTime,noteTxt,createdTime;
	  ImageView mImageView;
	  String alarmTime,note,createdDate;
	  int isAlarmSet,isDelEdt;
	  long idKey;
	  private DatabaseHelper mDatabaseHelper=new DatabaseHelper(this);
	  
	  public TaskDetail()
	  {
		 
	  }
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.taskdetail);
		schTime = (TextView) findViewById(R.id.schalarmdtl);
		noteTxt = (TextView) findViewById(R.id.notedtl);
		createdTime = (TextView) findViewById(R.id.createddtl);
//	    mTasks = mDatabaseHelper.getTask(idKey);
	    deleteBtn=(Button) findViewById(R.id.deletebtn);
	    editBtn=(Button) findViewById(R.id.editbtn);
	    mImageView=(ImageView) findViewById(R.id.alrmimgdtl);
	    
	    alarmTime = getIntent().getExtras().getString("AlarmTime");
	    note = getIntent().getExtras().getString("Note");
	    createdDate=getIntent().getExtras().getString("CreatedDate");
	    idKey=getIntent().getExtras().getLong("IdKey");
	    Log.i("idkeythis is",""+idKey);
	    isAlarmSet=getIntent().getExtras().getInt("IsAlarmSet");
	    isDelEdt=getIntent().getExtras().getInt("DeleteEdit");
	    schTime.setText(alarmTime);
	    noteTxt.setText(note);
	    createdTime.setText(createdDate);
	    if(isAlarmSet==1)
	    	mImageView.setVisibility(View.VISIBLE);
	    if(isDelEdt==0)
	    {   
	    	mDatabaseHelper.deleteToDo(idKey);
	    	deleteBtn.setVisibility(View.INVISIBLE);
	    	editBtn.setVisibility(View.INVISIBLE);
	    	Log.i("idkeythis is",""+idKey);
	    }
	    
	    deleteBtn.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				Log.i("listing",mDatabaseHelper.getAllTasks().toString());
				mDatabaseHelper.deleteToDo(idKey);
				
				Intent mIntent = new Intent(TaskDetail.this,ProfileActivity.class);
				startActivity(mIntent);
				finish();
			}
		});
	    editBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Intent mIntent = new Intent(TaskDetail.this,AddTaskActivity.class);
				mDatabaseHelper.deleteToDo(idKey);
				mIntent.putExtra("Note", note);
				mIntent.putExtra("IsAlarmSet", isAlarmSet);
				mIntent.putExtra("Flag", 1);
				startActivity(mIntent);
				finish();
				
			}
		});
	}
	public void onBackPressed() {
//		Intent intent = new Intent(TaskDetail.this, ProfileActivity.class);
//		startActivity(intent);
		finish();
		super.onBackPressed();
	}

}
