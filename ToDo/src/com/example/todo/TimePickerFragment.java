package com.example.todo;

import java.util.Calendar;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TextView;
import android.widget.TimePicker;

public class TimePickerFragment extends DialogFragment implements
		TimePickerDialog.OnTimeSetListener {
	Calendar calendar;
	int hour;
	int minute;
	int second;
	int amPm;
	Context context;
	AddTaskActivity mAddTask;
	DatabaseHelper mDatabaseHelper;
	TextView mTime;

	public TimePickerFragment(TextView date,AddTaskActivity addTask,DatabaseHelper databaseHelper) {
		mTime=date;
		mAddTask=addTask;
		mDatabaseHelper=databaseHelper;
	}
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current time as the default values for the picker
		calendar = Calendar.getInstance();
		hour = calendar.get(Calendar.HOUR_OF_DAY);
		minute = calendar.get(Calendar.MINUTE);
//		second = calendar.get(Calendar.SECOND);
		amPm = calendar.get(Calendar.AM_PM);

		// Create a new instance of TimePickerDialog and return it
		return new TimePickerDialog(getActivity(), this, hour, minute,
				DateFormat.is24HourFormat(getActivity()));
	}

	

	@Override
	public void onTimeSet(TimePicker view, int hour, int minute) {
		mTime.setText("Time:	"+ hour +" : " + minute + " : " + 0);
		mAddTask.getTime(hour,minute,0);
		mDatabaseHelper.getTime(hour,minute,0);

	}
}
